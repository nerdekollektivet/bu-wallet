/*
 * Designed and developed by 2020 skydoves (Jaewoong Eum)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.bitcoinunlimited.wallet.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped
import info.bitcoinunlimited.wallet.room.MnemonicDao
import info.bitcoinunlimited.wallet.ui.send.SendRepository
import info.bitcoinunlimited.wallet.utils.Constants
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@ExperimentalCoroutinesApi
@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@Module
@InstallIn(ViewModelComponent::class)
object RepositoryModule {

    @Provides
    @ViewModelScoped
    fun provideSendRepository(
        mnemonicDao: MnemonicDao,
        @ApplicationContext appContext: Context
    ): SendRepository {
        val chain = Constants.CURRENT_BLOCKCHAIN
        return SendRepository(
            chain,
            mnemonicDao,
            appContext
        )
    }

    @Provides
    @ViewModelScoped
    fun provideIdentityRepository(
        mnemonicDao: MnemonicDao,
        @ApplicationContext appContext: Context
    ): info.bitcoinunlimited.wallet.ui.receive.ReceiveRepository {
        return info.bitcoinunlimited.wallet.ui.receive.ReceiveRepository(
            Constants.CURRENT_BLOCKCHAIN,
            mnemonicDao,
            appContext
        )
    }
}
