package info.bitcoinunlimited.wallet.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import info.bitcoinunlimited.wallet.room.MnemonicDatabase
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @ExperimentalUnsignedTypes
    @Singleton
    @Provides
    fun provideDatabase(
        @ApplicationContext context: Context
    ): MnemonicDatabase {
        return Room.databaseBuilder(
            context,
            MnemonicDatabase::class.java,
            "bu_wallet_mnemonic_db"
        ).build()
    }

    @ExperimentalUnsignedTypes
    @Singleton
    @Provides
    fun provideDao(database: MnemonicDatabase) = database.mnemonicDao()
}
