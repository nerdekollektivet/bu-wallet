package info.bitcoinunlimited.wallet.ui

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.ui.ExperimentalComposeUiApi
import dagger.hilt.android.AndroidEntryPoint
import info.bitcoinunlimited.wallet.room.Mnemonic
import info.bitcoinunlimited.wallet.room.MnemonicDatabase
import info.bitcoinunlimited.wallet.utils.TAG_MAIN_ACTIVITY
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports

@ExperimentalCoroutinesApi
@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@InternalCoroutinesApi
@ExperimentalComposeUiApi
@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initPrivateKey()
    }

    private val handler = CoroutineExceptionHandler { _, exception ->
        Log.e(TAG_MAIN_ACTIVITY, "${MainActivity::class}: ${exception.message}")
        throw exception
    }

    private fun initPrivateKey() = GlobalScope.launch(Dispatchers.IO + handler) {
        val mnemonicDataBase = MnemonicDatabase.getInstance(applicationContext)
        val mnemonic = mnemonicDataBase.getMnemonic()
        renderUI(mnemonic)
    }

    private fun renderUI(mnemonic: Mnemonic) = GlobalScope.launch(Dispatchers.Main + handler) {
        setContent {
            MainScreen(mnemonic)
        }
    }
}
