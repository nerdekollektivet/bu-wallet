package info.bitcoinunlimited.wallet.ui.mnemonic

import androidx.compose.foundation.layout.* // ktlint-disable no-wildcard-imports
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.* // ktlint-disable no-wildcard-imports
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import info.bitcoinunlimited.wallet.R
import info.bitcoinunlimited.wallet.utils.TEST_INPUT_TAG

@ExperimentalComposeUiApi
@Composable
fun RecoverScreen() {
    Column(modifier = Modifier.fillMaxSize()) {
        MnemonicInput {
        }
        RecoverButton {
            // TODO: Recover mnemonic
        }
    }
}

@ExperimentalComposeUiApi
@Composable
fun MnemonicInput(onMnemonicChange: (TextFieldValue) -> Unit) {
    var textField by remember {
        mutableStateOf(
            TextFieldValue(
                text = ""
            )
        )
    }
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight(align = Alignment.Top)
            .padding(16.dp)
    ) {
        Text(stringResource(R.string.recover_replace_mnemonic_with_backup))
        OutlinedTextField(
            value = textField,
            label = { Text(stringResource(R.string.recover_mnemonic_label)) },
            modifier = Modifier.testTag(TEST_INPUT_TAG),
            keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Password),
            onValueChange = {
                textField = it
                onMnemonicChange(it)
            }
        )
    }
}

@Composable
fun RecoverButton(onClick: () -> Unit) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(align = Alignment.BottomCenter)
    ) {

        Button(onClick = onClick, modifier = Modifier.padding(64.dp)) {
            Text(stringResource(R.string.replace_mnemonic))
        }
    }
}

@ExperimentalComposeUiApi
@Preview
@Composable
fun RecoverScreenPreview() {
    RecoverScreen()
}
