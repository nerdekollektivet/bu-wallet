package info.bitcoinunlimited.wallet.ui.send

import android.content.Context
import bitcoinunlimited.libbitcoincash.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.wallet.network.ElectrumAPI
import info.bitcoinunlimited.wallet.room.MnemonicDao
import info.bitcoinunlimited.wallet.room.WalletDatabase
import info.bitcoinunlimited.wallet.utils.Constants
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@InternalCoroutinesApi
class SendRepository @Inject constructor(
    private val chain: ChainSelector,
    private val mnemonicDao: MnemonicDao,
    private val appContext: Context
) {
    private val electrumAPI = ElectrumAPI.getInstance(chain)

    suspend fun send(satoshis: Long, toAddress: String): String {
        val mnemonic = mnemonicDao.getMnemonic(Constants.mnemonicTableId)
            ?: throw Exception("Cannot get Mnemonic in RepositoryModule")
        val payDestination = WalletDatabase(appContext, mnemonic).getPayDestination()

        val toPayAddress = PayAddress(toAddress)
        val utxo = electrumAPI.getOneUtxo(toPayAddress, payDestination.secret, satoshis) ?: throw Error("Not enough funds")
        val input: BCHserializable = BCHinput(chain, utxo, BCHscript(chain))
        // val transaction = BCHtransaction(chain, input.BCHserialize())
        // val unspent = electrumAPI.listUnspent(toPayAddress)
        // TODO: Build transaction and broadcast payment
        // TODO: Get det same amount of unspent coins from BCHspendable as the satoshis parameters

        // electrumAPI.broadcast(transaction)

        println(toPayAddress)
        println(utxo)
        println(input)
        // println(transaction)
        // println(unspent)

        return "transactionIdMock"
    }

    fun parse(amount: String): Long {
        if (amount.contains(".") || amount.contains(',') || amount.isEmpty() ||
            amount.contains(' ') || amount.contains(" ")
        )
            throw Exception("Cannot enter character!")
        amount.forEach {
            if (it.isLetter()) {
                throw Exception("Cannot enter letter!")
            }
        }
        return amount.toLong()
    }
}
