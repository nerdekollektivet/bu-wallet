package info.bitcoinunlimited.wallet.ui.send

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.text.input.TextFieldValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.journeyapps.barcodescanner.ScanIntentResult
import dagger.hilt.android.lifecycle.HiltViewModel
import info.bitcoinunlimited.wallet.utils.TAG_RECEIVE
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import javax.inject.Inject

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
@HiltViewModel
class SendViewModel @Inject constructor(
    private val sendRepository: SendRepository,
) : ViewModel() {
    private val initialAddressText = "Scan a qr-code to send BCH"
    private val _toAddress = mutableStateOf(initialAddressText)
    private val _amountInput = mutableStateOf("")
    private val _error: MutableState<Exception?> = mutableStateOf(null)
    val toAddress: State<String>
        get() = _toAddress
    val amountInput: State<String>
        get() = _amountInput
    val error: State<Exception?>
        get() = _error

    private val handler = CoroutineExceptionHandler { _, error ->
        Log.e(TAG_RECEIVE, error.message ?: "Something went wrong in IdentityViewModel")
        _error.value = Exception(error)
    }

    internal fun setToAddress(address: String) {
        if (address.isNotEmpty())
            _toAddress.value = address
    }

    internal fun handleNewAmount(newAmountInput: TextFieldValue) {
        val newAmount = newAmountInput.text
        if (newAmount.contains(".") || newAmount.contains(',') ||
            newAmount.contains(' ') || newAmount.contains(" ") || newAmount.contains("-")
        )
            return
        newAmount.forEach {
            if (it.isLetter())
                return
        }
        if (newAmount.startsWith('0'))
            return
        _amountInput.value = newAmount
    }

    internal fun send(amount: String, toAddress: String, sendComplete: (String) -> Unit) {
        if (toAddress == initialAddressText) {
            _error.value = Exception("Scan a qr-code with a BCH address first!")
            return
        }
        viewModelScope.launch(Dispatchers.IO + handler) {
            val satoshis = sendRepository.parse(amount)
            val transactionId = sendRepository.send(satoshis, toAddress)
            viewModelScope.launch(Dispatchers.Main + handler) {
                sendComplete(transactionId)
            }
        }
    }

    internal fun onQrCodeScanned(scanIntentResult: ScanIntentResult) {
        val address: String? = scanIntentResult.contents
        if (address != null) {
            setToAddress(address)
        }
    }

    companion object {
        fun parse(amount: String): Long? {
            if (amount.contains(".") || amount.contains(',') || amount.isEmpty() ||
                amount.contains(' ') || amount.contains(" ")
            )
                return null
            amount.forEach {
                if (it.isLetter()) {
                    return null
                }
            }
            return amount.toLong()
        }
    }
}
