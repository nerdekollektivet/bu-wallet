package info.bitcoinunlimited.wallet.ui.mnemonic

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.* // ktlint-disable no-wildcard-imports
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import info.bitcoinunlimited.wallet.R
import info.bitcoinunlimited.wallet.room.Mnemonic
import info.bitcoinunlimited.wallet.utils.NAV_RECOVER

@Composable
fun MnemonicScreen(navController: NavController, viewModel: MnemonicViewModel) {
    Column(
        modifier = Modifier
            .fillMaxHeight()
            .background(colorResource(id = R.color.colorPrimaryDark))
            .padding(16.dp)
            .wrapContentSize(Alignment.TopCenter)
    ) {
        MnemonicPhraseWithSupportText(viewModel.mnemonic.value)
        NavigateToRecoverButton {
            navController.navigate(NAV_RECOVER)
        }
    }
}

@Composable
fun MnemonicPhraseWithSupportText(mnemonic: Mnemonic?) {
    Text(
        text = stringResource(R.string.mnemonic_headline),
        fontWeight = FontWeight.Bold,
        color = Color.White,
        textAlign = TextAlign.Center,
        fontSize = 25.sp
    )
    Text(
        text = mnemonic?.phrase ?: "...",
        fontWeight = FontWeight.Bold,
        color = Color.White,
        textAlign = TextAlign.Center,
    )
    Text(
        text = stringResource(R.string.mnemonic_security_warning),
        fontWeight = FontWeight.Light,
        color = Color.White,
        textAlign = TextAlign.Center,
    )
}

@Composable
fun NavigateToRecoverButton(onButtonClicked: () -> Unit) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(),
        verticalArrangement = Arrangement.Bottom,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Button(onClick = onButtonClicked, modifier = Modifier.fillMaxWidth()) {
            Text(text = NAV_RECOVER)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun MoviesScreenPreview() {
    MnemonicScreen(
        rememberNavController(),
        hiltViewModel()
    )
}
