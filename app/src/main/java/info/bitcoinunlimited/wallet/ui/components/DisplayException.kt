package info.bitcoinunlimited.wallet.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import info.bitcoinunlimited.wallet.R
import info.bitcoinunlimited.wallet.ui.theme.Accent
import info.bitcoinunlimited.wallet.utils.Faker

@Composable
fun DisplayException(exception: Exception) {
    Text(
        modifier = Modifier.background(Accent).padding(8.dp),
        fontSize = 16.sp,
        text = exception.localizedMessage ?: exception.message ?: stringResource(id = R.string.something_went_wrong)
    )
}

@Preview(showBackground = true)
@Composable
fun DisplayExceptionPreview() {
    DisplayException(exception = Faker().exception)
}
