package info.bitcoinunlimited.wallet.ui

import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import info.bitcoinunlimited.wallet.R
import info.bitcoinunlimited.wallet.room.Mnemonic
import info.bitcoinunlimited.wallet.ui.receive.ReceiveScreen
import info.bitcoinunlimited.wallet.ui.composables.SendCompleteScreen
import info.bitcoinunlimited.wallet.ui.send.SendScreen
import info.bitcoinunlimited.wallet.ui.mnemonic.MnemonicScreen
import info.bitcoinunlimited.wallet.ui.mnemonic.RecoverScreen
import info.bitcoinunlimited.wallet.utils.Faker
import info.bitcoinunlimited.wallet.utils.NAV_RECOVER
import info.bitcoinunlimited.wallet.utils.Routes.MNEMONIC
import info.bitcoinunlimited.wallet.utils.Routes.RECEIVE
import info.bitcoinunlimited.wallet.utils.Routes.SEND_COMPLETE
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

sealed class NavDrawerItem(var route: String, var icon: Int, var title: String) {
    class Receive : NavDrawerItem(RECEIVE, R.drawable.ic_receive, "Receive")
    class Send : NavDrawerItem("send", R.drawable.ic_send, "Send")
    data class Mnemonic(
        val mnemonicPhrase: String
    ) : NavDrawerItem("$MNEMONIC/$mnemonicPhrase", R.drawable.ic_baseline_verified_user_24, "Mnemonic")
}

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@ExperimentalComposeUiApi
@Composable
fun Navigation(navController: NavHostController, mnemonic: Mnemonic) {
    NavHost(
        navController = navController,
        startDestination = RECEIVE
    ) {
        composable(
            route = RECEIVE,
        ) {
            ReceiveScreen(hiltViewModel())
        }
        composable("send") {
            SendScreen(navController, hiltViewModel())
        }
        composable(
            "$SEND_COMPLETE/{amount}/{transactionId}",
            arguments = listOf(
                navArgument("amount") { type = NavType.IntType },
                navArgument("transactionId") { type = NavType.StringType }
            )
        ) {
            val amount = it.arguments?.getInt("amount")
                ?: throw Exception("Cannot get amount")
            val transactionId = it.arguments?.getString("transactionId")
                ?: throw Exception("Cannot get transaction id")
            SendCompleteScreen(
                amount,
                transactionId
            )
        }
        composable(
            route = "$MNEMONIC/{mnemonic_phrase}",
            arguments = listOf(
                navArgument("mnemonic_phrase") {
                    type = NavType.StringType
                    defaultValue = mnemonic.phrase
                }
            )
        ) {
            MnemonicScreen(navController, hiltViewModel())
        }
        composable(
            NAV_RECOVER
        ) {
            RecoverScreen()
        }
    }
}

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
@ExperimentalComposeUiApi
@Preview(showBackground = true)
@Composable
fun NavigationPreview() {
    Navigation(
        rememberNavController(),
        Faker().mnemonic,
    )
}
