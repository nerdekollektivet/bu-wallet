package info.bitcoinunlimited.wallet.ui.send

import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.compose.foundation.layout.* // ktlint-disable no-wildcard-imports
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.* // ktlint-disable no-wildcard-imports
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.journeyapps.barcodescanner.ScanContract
import com.journeyapps.barcodescanner.ScanOptions
import info.bitcoinunlimited.wallet.R
import info.bitcoinunlimited.wallet.ui.components.DisplayException
import info.bitcoinunlimited.wallet.utils.Routes.SEND_COMPLETE
import info.bitcoinunlimited.wallet.utils.TEST_INPUT_TAG
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@ExperimentalCoroutinesApi
@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@InternalCoroutinesApi
@ExperimentalComposeUiApi
@Composable
fun SendScreen(navController: NavController, sendVM: SendViewModel) {
    val amount: String by sendVM.amountInput
    val address: String by sendVM.toAddress
    val exception: Exception? by sendVM.error
    val scanLauncher = rememberLauncherForActivityResult(
        contract = ScanContract(),
        onResult = { sendVM.onQrCodeScanned(it) },
    )
    return Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        exception?.let { DisplayException(it) }
        SendScreenHeadline()
        BchAmountInput(
            onAmountChange = { newAmountInput -> sendVM.handleNewAmount(newAmountInput) }
        )
        QrButton {
            scanLauncher.launch(ScanOptions())
        }
        ToAddress(address)
        SendButton {
            sendVM.send(amount, address) { transactionId ->
                navController.navigate("$SEND_COMPLETE/$amount/$transactionId")
            }
        }
    }
}

@Composable
fun SendScreenHeadline() {
    Column(
        modifier = Modifier
            .wrapContentHeight()
            .fillMaxWidth()
            .padding(16.dp)
    ) {
        Text(
            text = stringResource(id = R.string.send_headline),
            fontWeight = FontWeight.Bold,
            color = Color.DarkGray,
            modifier = Modifier
                .padding(top = 25.dp),
            textAlign = TextAlign.Left,
            fontSize = 25.sp,
        )
    }
}

@Composable
fun ToAddress(address: String) {
    Text(text = address)
}

@Composable
fun SendButton(onButtonClicked: () -> Unit) {
    Button(onClick = onButtonClicked) {
        Text(text = stringResource(id = R.string.send))
    }
}

@ExperimentalComposeUiApi
@Composable
fun BchAmountInput(onAmountChange: (TextFieldValue) -> Unit) {
    val keyboardController = LocalSoftwareKeyboardController.current
    var textField by remember {
        mutableStateOf(
            TextFieldValue(
                text = ""
            )
        )
    }
    OutlinedTextField(
        modifier = Modifier.testTag(TEST_INPUT_TAG),
        singleLine = true,
        value = textField, // TextFieldValue(amount),
        label = { Text(text = "BCH amount (In satoshis)") },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number, imeAction = ImeAction.Done),
        keyboardActions = KeyboardActions(
            onDone = { keyboardController?.hide() }
        ),
        onValueChange = {
            textField = it
            onAmountChange(it)
        }
    )
}

@Composable
fun QrButton(onClick: () -> Unit) {
    Row(
        modifier = Modifier
            .wrapContentHeight()
            .fillMaxWidth()
    ) {

        Button(onClick = onClick) {
            Text(text = stringResource(R.string.scan_bch_address))
        }
    }
}

@Preview(showBackground = true)
@Composable
fun SendScreenHeadlinePreview() {
    SendScreenHeadline()
}

@ExperimentalComposeUiApi
@Preview(showBackground = true)
@Composable
fun QrButtonPreview() {
    QrButton() {
    }
}

@ExperimentalComposeUiApi
@Preview(showBackground = true)
@Composable
fun BchAmountInputPreview() {
    BchAmountInput() {
    }
}

@ExperimentalCoroutinesApi
@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@InternalCoroutinesApi
@ExperimentalComposeUiApi
@Preview(showBackground = true)
@Composable
fun SendScreenPreview() {
    SendScreen(
        rememberNavController(),
        hiltViewModel()
    )
}
