package info.bitcoinunlimited.wallet.ui

import android.util.Log
import androidx.compose.material.DrawerValue
import androidx.compose.material.Scaffold
import androidx.compose.material.rememberDrawerState
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.rememberNavController
import info.bitcoinunlimited.wallet.R
import info.bitcoinunlimited.wallet.room.Mnemonic
import info.bitcoinunlimited.wallet.ui.composables.TopBar
import info.bitcoinunlimited.wallet.utils.Faker
import info.bitcoinunlimited.wallet.utils.TAG_MAIN_ACTIVITY
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
@ExperimentalComposeUiApi
@Composable
fun MainScreen(mnemonic: Mnemonic) {
    val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Closed))
    val scope = rememberCoroutineScope()
    val navController = rememberNavController()

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = { TopBar(scope = scope, scaffoldState = scaffoldState) },
        drawerBackgroundColor = colorResource(id = R.color.colorPrimary),
        // scrimColor = Color.Red,  // Color for the fade background when you open/close the drawer
        drawerContent = {
            Drawer(scope, scaffoldState, navController, mnemonic)
        },
    ) {
        Navigation(navController = navController, mnemonic)
        Log.i(TAG_MAIN_ACTIVITY, it.toString())
    }
}

@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@ExperimentalComposeUiApi
@Preview(showBackground = true)
@Composable
fun MainScreenPreview() {
    MainScreen(Faker().mnemonic)
}
