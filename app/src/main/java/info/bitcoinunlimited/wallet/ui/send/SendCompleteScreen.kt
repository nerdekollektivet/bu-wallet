package info.bitcoinunlimited.wallet.ui.composables

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import info.bitcoinunlimited.wallet.R

@Composable
fun SendCompleteScreen(amount: Int, transactionId: String) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        Text(
            text = stringResource(R.string.send_complete),
            fontWeight = FontWeight.Bold,
            color = Color.DarkGray,
            modifier = Modifier
                .align(Alignment.Start)
                .padding(top = 25.dp),
            textAlign = TextAlign.Left,
            fontSize = 25.sp,
        )
        Text(
            text = "$amount ${stringResource(R.string.satosish_sent_bch)}",
            fontWeight = FontWeight.Bold,
            color = Color.DarkGray,
            modifier = Modifier
                .align(Alignment.Start)
                .padding(top = 25.dp),
            textAlign = TextAlign.Left,
            fontSize = 25.sp,
        )
        Text(
            text = "${stringResource(R.string.transaction_id)}$transactionId",
            fontWeight = FontWeight.Bold,
            color = Color.DarkGray,
            modifier = Modifier
                .align(Alignment.Start)
                .padding(top = 25.dp),
            textAlign = TextAlign.Left,
            fontSize = 25.sp,
        )
    }
}

@Preview(showBackground = true)
@Composable
fun SendCompleteScreenPreview() {
    SendCompleteScreen(
        10000, "transactionIdMock"
    )
}
