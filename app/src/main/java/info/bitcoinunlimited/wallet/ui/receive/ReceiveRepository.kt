package info.bitcoinunlimited.wallet.ui.receive

import android.content.Context
import bitcoinunlimited.libbitcoincash.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.wallet.network.ElectrumAPI
import info.bitcoinunlimited.wallet.room.MnemonicDao
import info.bitcoinunlimited.wallet.room.WalletDatabase
import info.bitcoinunlimited.wallet.utils.Constants
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import javax.inject.Inject

@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class ReceiveRepository @Inject constructor(
    chain: ChainSelector,
    private val mnemonicDao: MnemonicDao,
    private val appContext: Context
) {
    private val electrumAPI: ElectrumAPI = ElectrumAPI.getInstance(chain)

    internal fun getAddress(): PayAddress? {
        val mnemonic = mnemonicDao.getMnemonic(Constants.mnemonicTableId)
            ?: throw Exception("Cannot get Mnemonic in RepositoryModule")
        val payDestination = WalletDatabase(appContext, mnemonic).getPayDestination()
        return payDestination.address
    }

    internal suspend fun getBalance(list: List<PayAddress>): Int {
        var balance = 0
        list.forEach {
            val result = electrumAPI.getBalance(it)
            balance += result.confirmed
            balance += result.unconfirmed
        }
        return balance
    }
    // TODO: Get balance for all addresses
    internal fun subscribeAddress(addressHuman: String, onEvent: () -> Unit) {
        electrumAPI.subscribeToAddress(addressHuman) {
            onEvent()
        }
    }

    internal fun unsubscribeAddress(addressHuman: String) {
        electrumAPI.unsubscribeAddress(addressHuman)
    }
}
