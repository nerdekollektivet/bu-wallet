package info.bitcoinunlimited.wallet.ui.mnemonic

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import info.bitcoinunlimited.wallet.room.Mnemonic
import info.bitcoinunlimited.wallet.utils.TAG_MNEMONIC
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MnemonicViewModel @Inject constructor(
    private val mnemonicRepository: MnemonicRepository,
) : ViewModel() {
    private val _mnemonic: MutableState<Mnemonic?> = mutableStateOf(null)
    val mnemonic: State<Mnemonic?>
        get() = _mnemonic

    internal val handler = CoroutineExceptionHandler { _, exception ->
        Log.e(TAG_MNEMONIC, exception.message ?: "Something went wrong in MnemonicViewModel")
    }

    init {
        viewModelScope.launch(Dispatchers.IO + handler) {
            val mnemonic = mnemonicRepository.getMnemonic()
            viewModelScope.launch(Dispatchers.Main + handler) {
                _mnemonic.value = mnemonic
            }
        }
    }
}
