/*
 * Copyright (c) 2021 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * This project and source code may use libraries or frameworks that are
 * released under various Open-Source licenses. Use of those libraries and
 * frameworks are governed by their own individual licenses.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package info.bitcoinunlimited.wallet.ui.receive

import android.graphics.Bitmap
import android.graphics.Color
import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import bitcoinunlimited.libbitcoincash.PayAddress
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter
import dagger.hilt.android.lifecycle.HiltViewModel
import info.bitcoinunlimited.wallet.utils.TAG_RECEIVE
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import javax.inject.Inject

@ExperimentalCoroutinesApi
@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@HiltViewModel
class ReceiveViewModel @Inject constructor(
    private val receiveRepository: ReceiveRepository,
) : ViewModel() {
    lateinit var addressHuman: String
    private val _address: MutableState<String> = mutableStateOf("""...""")
    private val _qrCode: MutableState<ImageBitmap?> = mutableStateOf(null)
    private val _balance: MutableState<Int?> = mutableStateOf(null)
    val address: State<String>
        get() = _address
    val qrCode: State<ImageBitmap?>
        get() = _qrCode
    val balance: State<Int?> get() = _balance

    private val handler = CoroutineExceptionHandler { _, exception ->
        Log.e(TAG_RECEIVE, exception.message ?: "Something went wrong in IdentityViewModel")
    }

    init {
        viewModelScope.launch(Dispatchers.IO + handler) {
            val newAddress = receiveRepository.getAddress()
                ?: throw Exception("cannot get newAddress")
            _address.value = newAddress.toString() // Possible concurrency issue here
            addressHuman = newAddress.toString()
            _qrCode.value = generateQrCode(addressHuman)
            subscribeAddress(addressHuman)
            fetchBalance(listOf(newAddress))
        }
    }

    // TODO: Subscribe to all addresses with UTXOs
    private fun subscribeAddress(address: String) {
        receiveRepository.subscribeAddress(address) {
            // TODO: Get the balance of each address.
        }
    }

    private suspend fun fetchBalance(addresses: List<PayAddress>) {
        // TODO: Display progressbar
        val balance = receiveRepository.getBalance(addresses)
        setBalance(balance)
        // TODO: Hide progressbar
    }

    private fun setBalance(balance: Int) = viewModelScope.launch(
        Dispatchers.Main + handler
    ) {
        _balance.value = balance
    }

    internal fun unsubscribeAddress() {
        receiveRepository.unsubscribeAddress(addressHuman)
    }

    companion object {
        internal fun generateQrCode(address: String): ImageBitmap {
            if (address.isEmpty()) throw IllegalArgumentException("Address is empty!")
            val writer = QRCodeWriter()
            val bitMatrix = writer.encode(address, BarcodeFormat.QR_CODE, 346, 346)
            val bitmap = Bitmap.createBitmap(bitMatrix.width, bitMatrix.height, Bitmap.Config.RGB_565)
            for (x in 0 until bitMatrix.width) {
                for (y in 0 until bitMatrix.height) {
                    bitmap.setPixel(x, y, if (bitMatrix.get(x, y)) Color.BLACK else Color.WHITE)
                }
            }
            return bitmap.asImageBitmap()
        }
    }
}
