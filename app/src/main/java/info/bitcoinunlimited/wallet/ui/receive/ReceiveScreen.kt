package info.bitcoinunlimited.wallet.ui.receive

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.* // ktlint-disable no-wildcard-imports
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.ClipboardManager
import androidx.compose.ui.platform.LocalClipboardManager
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat.startActivity
import androidx.hilt.navigation.compose.hiltViewModel
import info.bitcoinunlimited.wallet.R
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
@Composable
fun ReceiveScreen(receiveViewModel: ReceiveViewModel) {
    val qrCode = receiveViewModel.qrCode.value
    val context = LocalContext.current
    val clipboardManager = LocalClipboardManager.current
    val balance: Int? by receiveViewModel.balance
    val address = receiveViewModel.address.value

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        // LinearProgressIndicator()
        qrCode?.let {
            QrCode(it)
        }
        Balance(
            balance ?: 0
        )
        Address(address) {
            copyAddress(address, clipboardManager, context)
        }
        ShareAddress() {
            val intent = getShareIntent(address)
            share(intent, context)
        }
    }
}

private fun copyAddress(address: String, clipboardManager: ClipboardManager, context: Context) {
    clipboardManager.setText(AnnotatedString(address))
    Toast.makeText(context, context.getString(R.string.copy_address_to_clipboard), Toast.LENGTH_LONG).show()
}

private fun getShareIntent(address: String): Intent {
    val type = "text/plain"
    val subject = "Share address"
    val shareWith = "ShareWith"
    val intent = Intent(Intent.ACTION_SEND)

    intent.type = type
    intent.putExtra(Intent.EXTRA_SUBJECT, subject)
    intent.putExtra(Intent.EXTRA_TEXT, address)
    return Intent.createChooser(intent, shareWith)
        .setFlags(FLAG_ACTIVITY_NEW_TASK)
}

private fun share(chooser: Intent, context: Context) {
    startActivity(
        context.applicationContext,
        chooser,
        null
    )
}

@Composable
fun QrCode(qrCode: ImageBitmap) {
    Image(
        bitmap = qrCode,
        contentDescription = stringResource(R.string.qr_code_with_address),
        contentScale = ContentScale.FillBounds,
        modifier = Modifier
            .height(346.dp)
            .width(346.dp)
    )
}

@Composable
fun Address(address: String, onTap: ((Offset) -> Unit)) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(16.dp)
            .pointerInput(Unit) {
                detectTapGestures(onTap = onTap)
            }
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_outline_file_copy_24),
            contentDescription = stringResource(id = R.string.copy_address),
            contentScale = ContentScale.FillWidth,
            modifier = Modifier.padding(end = 8.dp)
        )
        Text(
            text = address,
            fontSize = 16.sp
        )
    }
}

@Composable
fun Balance(balance: Int) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(16.dp)
    ) {
        Image(
            painter = painterResource(id = R.drawable.bitcoin_cash_circle),
            contentDescription = stringResource(R.string.bitcoin_cash_logo),
            contentScale = ContentScale.FillWidth,
            modifier = Modifier
                .width(35.dp)
                .padding(end = 8.dp)
        )
        Text(text = balance.toString(), fontSize = 22.sp)
    }
}

@Composable
fun ShareAddress(onTap: (Offset) -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(16.dp)
            .pointerInput(Unit) {
                detectTapGestures(onTap = onTap)
            }
    ) {
        Image(
            modifier = Modifier.padding(end = 8.dp),
            painter = painterResource(id = R.drawable.ic_menu_share),
            contentDescription = stringResource(R.string.share_address),
            contentScale = ContentScale.FillWidth
        )
        Text(text = stringResource(R.string.share_address), fontSize = 22.sp, textAlign = TextAlign.Center)
    }
}

@Preview(showBackground = true)
@Composable
fun BalancePreview() {
    Balance(1000)
}

@Preview(showBackground = true)
@Composable
fun ShareAddressPreview() {
    ShareAddress {
    }
}

@Preview(showBackground = true)
@Composable
fun AddressPreview() {
    Address(address = "asd") {
    }
}

@ExperimentalCoroutinesApi
@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@Preview(showBackground = true)
@Composable
fun ReceiveScreenPreview() {
    ReceiveScreen(hiltViewModel())
}
