package info.bitcoinunlimited.wallet.ui.mnemonic

import info.bitcoinunlimited.wallet.room.Mnemonic
import info.bitcoinunlimited.wallet.room.MnemonicDao
import info.bitcoinunlimited.wallet.utils.Constants
import javax.inject.Inject

class MnemonicRepository @Inject constructor (private val mnemonicDao: MnemonicDao) {

    fun getMnemonic(): Mnemonic {
        return mnemonicDao.getMnemonic(Constants.mnemonicTableId)
            ?: throw Exception("Cannot get Mnemonic in MnemonicRepository")
    }
}
