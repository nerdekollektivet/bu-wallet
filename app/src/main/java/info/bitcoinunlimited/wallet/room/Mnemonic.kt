package info.bitcoinunlimited.wallet.room

import androidx.room.Entity
import androidx.room.PrimaryKey
import info.bitcoinunlimited.wallet.utils.MNEMONIC_TABLE_ID
import java.io.Serializable

@Entity(tableName = "mnemonic_table")
data class Mnemonic(
    val phrase: String,
    @PrimaryKey
    val tableId: String = MNEMONIC_TABLE_ID
) : Serializable {
    constructor() : this("INVALID")
}
