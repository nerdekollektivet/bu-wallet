package info.bitcoinunlimited.wallet.utils

import bitcoinunlimited.libbitcoincash.ChainSelector

/**
 * Constants for Bitcoin Unlimited Wallet
 */
object Constants {
    val CURRENT_BLOCKCHAIN: ChainSelector = ChainSelector.BCHMAINNET
    const val mnemonicTableId = "mnemonic"
}
const val TEST_INPUT_TAG = "TEST_INPUT_TAG"

const val NAV_RECOVER = "recover"

object Routes {
    const val SEND_COMPLETE = "send_complete"
    const val MNEMONIC = "mnemonic"
    const val RECEIVE = "receive"
}

const val MNEMONIC_TABLE_ID = "mnemonic"
