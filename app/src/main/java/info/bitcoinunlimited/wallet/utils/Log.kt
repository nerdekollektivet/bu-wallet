package info.bitcoinunlimited.wallet.utils

const val TAG_RECEIVE = "TAG_RECEIVE"
const val TAG_MNEMONIC = "TAG_MNEMONIC"
const val TAG_SEND = "TAG_SEND"
const val TAG_WALLET_REPOSITORY = "TAG_WALLET_REPOSITORY"
const val TAG_WALLET_ACTIVITY = "TAG_WALLET_ACTIVITY"
const val TAG_MAIN_ACTIVITY = "TAG_MAIN_ACTIVITY"
const val TAG_ELECTRUM_API = "TAG_ELECTRUM_API"
