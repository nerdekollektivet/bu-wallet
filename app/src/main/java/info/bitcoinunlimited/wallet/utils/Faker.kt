package info.bitcoinunlimited.wallet.utils

import info.bitcoinunlimited.wallet.room.Mnemonic
import java.lang.Exception

data class Faker(
    val mnemonicPhrase: String = "word1 word2 word3 word4 word5 word6 word7 word8 word9 word10 word11 word12",
    val mnemonic: Mnemonic = Mnemonic(mnemonicPhrase),
    val exception: Exception = Exception("This is a fake error message!"),
)
