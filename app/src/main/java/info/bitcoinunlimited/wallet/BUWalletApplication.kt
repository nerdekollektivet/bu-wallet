package info.bitcoinunlimited.wallet

import android.app.Application
import bitcoinunlimited.libbitcoincash.Initialize
import dagger.hilt.android.HiltAndroidApp
import info.bitcoinunlimited.wallet.utils.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
@HiltAndroidApp
open class BUWalletApplication : Application() {
    companion object {
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(Constants.CURRENT_BLOCKCHAIN.v)
        }
    }
}
