package info.bitcoinunlimited.wallet.room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import info.bitcoinunlimited.wallet.Mock
import info.bitcoinunlimited.wallet.utils.Constants
import java.io.IOException
import kotlin.jvm.Throws
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test

@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
class MnemonicDaoTest {
    private lateinit var mnemonicDao: MnemonicDao
    private lateinit var db: MnemonicDatabase

    @Before fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, MnemonicDatabase::class.java).build()
        mnemonicDao = db.mnemonicDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertAndGetMnemonicTest() = runBlocking {
        val Mock = Mock.getMnemonic()
        mnemonicDao.insert(Mock)
        val currentMnemonic = mnemonicDao.getMnemonic(Constants.mnemonicTableId)
        assertThat(currentMnemonic, equalTo(Mock))
    }
}
