package info.bitcoinunlimited.wallet.ui.mnemonic

import androidx.compose.ui.res.stringResource
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import info.bitcoinunlimited.wallet.R
import info.bitcoinunlimited.wallet.utils.Faker
import info.bitcoinunlimited.wallet.utils.NAV_RECOVER
import org.junit.Rule
import org.junit.Test

class MnemonicScreenTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private val mnemonic = Faker().mnemonic
    private val mnemonicPhrase = mnemonic.phrase

    @Test
    fun mnemonicScreenTest_mnemonicPhraseWithSupportText() {
        lateinit var mnemonicHeadline: String
        lateinit var mnemonicWarning: String
        composeTestRule.setContent {
            mnemonicHeadline = stringResource(R.string.mnemonic_headline)
            mnemonicWarning = stringResource(R.string.mnemonic_security_warning)
            MnemonicPhraseWithSupportText(mnemonic)
        }

        composeTestRule.onNodeWithText(mnemonicHeadline).assertIsDisplayed()
        composeTestRule.onNodeWithText(mnemonicPhrase).assertIsDisplayed()
        composeTestRule.onNodeWithText(mnemonicWarning).assertIsDisplayed()
    }

    @Test
    fun mnemonicScreenTest_navigateToRecoverButton() {
        var clicked = false
        composeTestRule.setContent {
            NavigateToRecoverButton() {
                clicked = true
            }
        }
        composeTestRule.onNodeWithText(NAV_RECOVER).assertIsDisplayed()
        composeTestRule.onNodeWithText(NAV_RECOVER).performClick()
        assert(clicked)
    }
}
