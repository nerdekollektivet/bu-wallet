package info.bitcoinunlimited.wallet.ui

import androidx.compose.material.DrawerValue
import androidx.compose.material.rememberDrawerState
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.navigation.compose.rememberNavController
import info.bitcoinunlimited.wallet.utils.Faker
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@ExperimentalComposeUiApi
@InternalCoroutinesApi
class DrawerTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    private val mnemonic = Faker().mnemonic

    @Test
    fun drawer_defaultIsDisplaying() {
        val items = listOf(
            NavDrawerItem.Receive(),
            NavDrawerItem.Send(),
            NavDrawerItem.Mnemonic(mnemonic.phrase),
        )
        composeTestRule.setContent {
            val scope = rememberCoroutineScope()
            val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Closed))
            val navController = rememberNavController()
            Drawer(scope, scaffoldState, navController, mnemonic)
        }

        items.forEach {
            composeTestRule.onNodeWithText(it.title).assertIsDisplayed()
        }
    }
}
