package info.bitcoinunlimited.wallet.ui.components

import androidx.compose.ui.res.stringResource
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import info.bitcoinunlimited.wallet.R
import org.junit.Rule
import org.junit.Test

class DisplayExceptionTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun displayException_customMessage() {
        val message = "Something went wrong! This is a mock!"
        val exception = Exception(message)
        composeTestRule.setContent {
            DisplayException(exception = exception)
        }
        composeTestRule.onNodeWithText(message).assertIsDisplayed()
    }

    @Test
    fun displayException_default() {
        lateinit var message: String
        composeTestRule.setContent {
            message = stringResource(id = R.string.something_went_wrong)
            DisplayException(exception = Exception())
        }
        composeTestRule.onNodeWithText(message).assertIsDisplayed()
    }
}
