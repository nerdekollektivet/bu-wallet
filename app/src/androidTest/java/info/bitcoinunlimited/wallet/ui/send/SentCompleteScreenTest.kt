package info.bitcoinunlimited.wallet.ui.send

import androidx.compose.ui.res.stringResource
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import info.bitcoinunlimited.wallet.ui.composables.SendCompleteScreen
import org.junit.Rule
import org.junit.Test
import info.bitcoinunlimited.wallet.R

class SentCompleteScreenTest {
    @get:Rule
    val composeTestRule = createComposeRule()
    private val amount = 10000

    @Test
    fun sendCompleteScreenTest() {
        val transactionId = "mockTransactionId"
        lateinit var sendComplete: String
        lateinit var sentBch: String
        lateinit var transaction: String

        composeTestRule.setContent {
            sendComplete = stringResource(id = R.string.send_complete)
            sentBch = "$amount ${stringResource(R.string.satosish_sent_bch)}"
            transaction = "${stringResource(R.string.transaction_id)}$transactionId"
            SendCompleteScreen(amount = amount, transactionId = transactionId)
        }

        composeTestRule.onNodeWithText(sendComplete).assertIsDisplayed()
        composeTestRule.onNodeWithText(sentBch).assertIsDisplayed()
        composeTestRule.onNodeWithText(transaction).assertIsDisplayed()
    }
}
