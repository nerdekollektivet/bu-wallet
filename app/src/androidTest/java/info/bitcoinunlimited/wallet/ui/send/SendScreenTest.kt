package info.bitcoinunlimited.wallet.ui.send

import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.test.* // ktlint-disable no-wildcard-imports
import androidx.compose.ui.test.junit4.createComposeRule
import org.junit.Rule
import org.junit.Test
import info.bitcoinunlimited.wallet.R
import info.bitcoinunlimited.wallet.utils.TEST_INPUT_TAG

class SendScreenTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun sendScreenTest_qrButton() {
        lateinit var text: String
        var clicked = 0
        composeTestRule.setContent {
            text = stringResource(id = R.string.scan_bch_address)
            QrButton() {
                clicked++
            }
        }

        composeTestRule.onNodeWithText(text).assertIsDisplayed().performClick()
        assert(clicked == 1)
    }

    @Test
    fun sendScreenTest_headline() {
        lateinit var text: String
        composeTestRule.setContent {
            text = stringResource(id = R.string.send_headline)
            SendScreenHeadline()
        }

        composeTestRule.onNodeWithText(text).assertIsDisplayed()
    }

    @Test
    @ExperimentalComposeUiApi
    fun sendScreenTest_BchAmountInput() {
        val input1 = "1000"
        val input2 = "400000"
        var output = ""
        composeTestRule.setContent {
            BchAmountInput() {
                output = it.text
            }
        }

        composeTestRule.onNodeWithTag(TEST_INPUT_TAG).assertIsDisplayed()
        composeTestRule.onNodeWithTag(TEST_INPUT_TAG).performTextInput(input1)
        composeTestRule.onNodeWithTag(TEST_INPUT_TAG).assertTextContains(input1)
        assert(output == input1)
        composeTestRule.onNodeWithTag(TEST_INPUT_TAG).performTextClearance()
        composeTestRule.onNodeWithTag(TEST_INPUT_TAG).performTextInput(input2)
        composeTestRule.onNodeWithTag(TEST_INPUT_TAG).assertTextContains(input2)
        assert(output == input2)
    }
}
