package info.bitcoinunlimited.wallet.ui

import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import info.bitcoinunlimited.wallet.utils.Faker
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@ExperimentalComposeUiApi
@InternalCoroutinesApi
class DrawerItemTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    private val mnemonic = Faker().mnemonic

    @Test
    fun drawerItem_defaultIsDisplaying() {
        val item = NavDrawerItem.Mnemonic(mnemonic.phrase)

        composeTestRule.setContent {
            DrawerItem(
                item = item,
                selected = false,
                onItemClick = { }
            )
        }

        composeTestRule.onNodeWithText(item.title).assertIsDisplayed()
    }

    @Test
    fun drawerItem_isClickable() {
        val item = NavDrawerItem.Mnemonic(mnemonic.phrase)
        var clicked = false

        composeTestRule.setContent {
            DrawerItem(item = item, selected = false, onItemClick = {
                clicked = true
            })
        }

        composeTestRule.onNodeWithText(item.title).assertIsDisplayed().performClick()
        assert(clicked)
    }
}
