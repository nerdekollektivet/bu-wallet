package info.bitcoinunlimited.wallet.ui.mnemonic

import io.mockk.mockk
import org.junit.Assert.* // ktlint-disable no-wildcard-imports
import org.junit.Before
import org.junit.Test

class MnemonicViewModelTest {

    private lateinit var mnemonicRepository: MnemonicRepository
    private lateinit var mnemonicViewModel: MnemonicViewModel

    @Before
    fun setUp() {
        mnemonicRepository = mockk()
        mnemonicViewModel = MnemonicViewModel(mnemonicRepository)
    }

    @Test
    fun initMnemonicNull() {
        assertEquals(mnemonicViewModel.mnemonic.value, null)
    }
}
