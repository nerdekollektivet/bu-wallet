package info.bitcoinunlimited.wallet.ui.receive

import androidx.compose.ui.res.stringResource
import androidx.compose.ui.test.* // ktlint-disable no-wildcard-imports
import androidx.compose.ui.test.junit4.createComposeRule
import info.bitcoinunlimited.wallet.R
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import org.junit.Rule
import org.junit.Test

class ReceiveScreenTest {

    @get:Rule
    val composeTestRule = createComposeRule()
    private val addressMock = "bitcoincash:qqv4urhsv7psuaep04kdq5tpp35v22x9mvdszf7prh"

    @ExperimentalUnsignedTypes
    @DelicateCoroutinesApi
    @ExperimentalCoroutinesApi
    @InternalCoroutinesApi
    @Test
    fun receiveScreenTest_qrCode() {
        lateinit var contentDescription: String
        composeTestRule.setContent {
            contentDescription = stringResource(R.string.qr_code_with_address)
            QrCode(qrCode = ReceiveViewModel.generateQrCode(addressMock))
        }
        composeTestRule.onNodeWithContentDescription(contentDescription).assertIsDisplayed()
    }

    @Test
    fun receiveScreenTest_address() {
        var copyAddressClicked = 0
        var copyAddress = ""
        composeTestRule.setContent {
            copyAddress = stringResource(R.string.copy_address)
            Address(addressMock) {
                copyAddressClicked++
            }
        }
        composeTestRule.onNodeWithText(addressMock).assertIsDisplayed().performClick()
        assert(copyAddressClicked == 1)
        composeTestRule.onNodeWithContentDescription(copyAddress).assertIsDisplayed().performClick()
        assert(copyAddressClicked == 2)
    }

    @Test
    fun receiveScreenTest_shareAddress() {
        var shareAddressClicked = 0
        var shareAddress = ""
        composeTestRule.setContent {
            shareAddress = stringResource(id = R.string.share_address)
            ShareAddress() {
                shareAddressClicked++
            }
        }

        composeTestRule.onNodeWithText(shareAddress).assertIsDisplayed().performClick()
        assert(shareAddressClicked == 1)
        composeTestRule.onNodeWithContentDescription(shareAddress).assertIsDisplayed().performClick()
        assert(shareAddressClicked == 2)
    }

    @Test
    fun receiveScreenTest_balance() {
        val balanceMock = 10000
        lateinit var shareAddress: String
        composeTestRule.setContent {
            shareAddress = stringResource(id = R.string.bitcoin_cash_logo)
            Balance(balance = balanceMock)
        }
        composeTestRule.onNodeWithText(balanceMock.toString()).assertIsDisplayed()
        composeTestRule.onNodeWithContentDescription(shareAddress).assertIsDisplayed()
    }
}
