package info.bitcoinunlimited.wallet.ui.mnemonic

import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.test.* // ktlint-disable no-wildcard-imports
import androidx.compose.ui.test.junit4.createComposeRule
import info.bitcoinunlimited.wallet.R
import info.bitcoinunlimited.wallet.utils.TEST_INPUT_TAG
import org.junit.Rule
import org.junit.Test

class RecoverScreenTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun recoverScreenTest_recoverButton() {
        var clicked = false
        lateinit var replaceMnemonic: String
        composeTestRule.setContent {
            replaceMnemonic = stringResource(id = R.string.replace_mnemonic)
            RecoverButton() {
                clicked = true
            }
        }
        composeTestRule.onNodeWithText(replaceMnemonic).assertIsDisplayed()
        composeTestRule.onNodeWithText(replaceMnemonic).performClick()
        assert(clicked)
    }

    @Test
    @ExperimentalComposeUiApi
    fun recoverScreenTest_mnemonicInput() {
        composeTestRule.setContent {
            MnemonicInput() {
                // TODO: Verify UI Change on new input
            }
        }
        composeTestRule.onNodeWithTag(TEST_INPUT_TAG).assertIsDisplayed()
    }
}
