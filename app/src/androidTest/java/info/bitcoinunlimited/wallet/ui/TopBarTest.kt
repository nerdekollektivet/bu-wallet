package info.bitcoinunlimited.wallet.ui

import androidx.compose.material.DrawerValue
import androidx.compose.material.rememberDrawerState
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import info.bitcoinunlimited.wallet.ui.composables.TopBar
import org.junit.Rule

import org.junit.Test

class TopBarTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun topBarTest_defaultTextDisplaying() {
        composeTestRule.setContent {
            val scope = rememberCoroutineScope()
            val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Closed))
            TopBar(scope = scope, scaffoldState = scaffoldState)
        }

        composeTestRule.onNodeWithText("Bitcoin Unlimited Wallet").assertIsDisplayed()
    }
}
