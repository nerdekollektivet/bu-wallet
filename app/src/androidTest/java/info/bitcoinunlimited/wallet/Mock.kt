package info.bitcoinunlimited.wallet

import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Hash
import bitcoinunlimited.libbitcoincash.PayAddress
import bitcoinunlimited.libbitcoincash.PayAddressType
import bitcoinunlimited.libbitcoincash.PayDestination
import info.bitcoinunlimited.wallet.room.Mnemonic

object Mock {
    fun mockPrivateKey(fill: Char = 'A'): ByteArray = ByteArray(32) { _ -> fill.toByte() }

    fun mockP2pkhAddress(chain: ChainSelector, privateKey: ByteArray): PayAddress {
        val pkh = toPKH(PayDestination.GetPubKey(privateKey))
        return PayAddress(chain, PayAddressType.P2PKH, pkh)
    }

    private fun toPKH(publicKey: ByteArray): ByteArray {
        return Hash.hash160(publicKey)
    }
    fun getMnemonic(): Mnemonic {
        return Mnemonic("cupboard two beach document doctor toward behave attend cliff mesh dove stool")
    }
}
