package info.bitcoinunlimited.wallet

import info.bitcoinunlimited.wallet.room.Mnemonic
import info.bitcoinunlimited.wallet.utils.Constants.mnemonicTableId
import info.bitcoinunlimited.wallet.utils.Faker
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class MnemonicTest {

    private lateinit var mnemonic: Mnemonic

    @Before fun setUp() {
        mnemonic = Faker().mnemonic
    }

    @Test fun test_default_value() {
        val phrase = mnemonic.phrase
        val defaultMnemonic = Mnemonic(phrase)
        assertEquals(phrase, defaultMnemonic.phrase)
        assertEquals("mnemonic", mnemonicTableId)
    }
}
