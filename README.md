# Bitcoin Unlimited (BU) Wallet

<a align="center" href="https://gitlab.com/nerdekollektivet/bu-wallet/-/releases">
  Releases - Download latest .apk directly
</a>

### Get keystore hash

To get the $signing_jks_file_hex env variable hex value, we use this binary -> hex command: 'xxd -p gitter-android-app.jks'